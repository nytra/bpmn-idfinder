﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Serialization;

namespace BPMNanalyzer
{
    class Program
    {
        static int counter = 0;
        static readonly string bpmnPath = @"C:\Users\christian.nytra\Desktop\BPMNdiagrams\";
        static string trennString = "________________________________|_______________________________________|_____________________________________";

        static void Main(string[] args)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(definitions));
            List<definitions> XMLcontents = new List<definitions>();
            List<string> fileNames = new List<string>();

            foreach (string file in Directory.EnumerateFiles(bpmnPath, "*bpmn"))
            {
                fileNames.Add(file.Replace(bpmnPath, ""));
                using (FileStream fileStream = new FileStream(file, FileMode.Open))
                {
                    XMLcontents.Add((definitions)serializer.Deserialize(fileStream));
                }
            }

            for (int i = 0; i < XMLcontents.Count; i++)
            {
                Console.WriteLine($"\nFilename:\t{fileNames[i]}\n");

                if (counter > 0)
                {
                    counter = 0;
                }

                foreach (var entry in XMLcontents[i].process.Items)
                {
                    if (counter == 0)
                    {
                        Console.WriteLine($"Task Id's:\t{entry.id}\t|ingoing Id's:\t{entry.incoming}\t|outgoing Id's:\t{entry.outgoing}");
                    }
                    else
                    {
                        Console.WriteLine($"\t\t{entry.id}\t|\t\t{entry.incoming}\t|\t\t{entry.outgoing}");
                    }
                        counter++;
                }
                Console.WriteLine(trennString);
            }
        }
    }
}
