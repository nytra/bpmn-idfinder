﻿
/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.omg.org/spec/BPMN/20100524/DI")]
[System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.omg.org/spec/BPMN/20100524/DI", IsNullable = false)]
public partial class BPMNDiagram
{

    private BPMNDiagramBPMNPlane bPMNPlaneField;

    private string idField;

    /// <remarks/>
    public BPMNDiagramBPMNPlane BPMNPlane
    {
        get
        {
            return this.bPMNPlaneField;
        }
        set
        {
            this.bPMNPlaneField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string id
    {
        get
        {
            return this.idField;
        }
        set
        {
            this.idField = value;
        }
    }
}

