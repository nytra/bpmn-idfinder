﻿
// NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")]
[System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL", IsNullable = false)]
public partial class definitions
{

    private definitionsProcess processField;

    private BPMNDiagram bPMNDiagramField;

    private string idField;

    private string targetNamespaceField;

    private string exporterField;

    private string exporterVersionField;

    /// <remarks/>
    public definitionsProcess process
    {
        get
        {
            return this.processField;
        }
        set
        {
            this.processField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.omg.org/spec/BPMN/20100524/DI")]
    public BPMNDiagram BPMNDiagram
    {
        get
        {
            return this.bPMNDiagramField;
        }
        set
        {
            this.bPMNDiagramField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string id
    {
        get
        {
            return this.idField;
        }
        set
        {
            this.idField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string targetNamespace
    {
        get
        {
            return this.targetNamespaceField;
        }
        set
        {
            this.targetNamespaceField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string exporter
    {
        get
        {
            return this.exporterField;
        }
        set
        {
            this.exporterField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string exporterVersion
    {
        get
        {
            return this.exporterVersionField;
        }
        set
        {
            this.exporterVersionField = value;
        }
    }
}

