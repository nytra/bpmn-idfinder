﻿
/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")]
public partial class definitionsProcessServiceTask
{

    private string incomingField;

    private string outgoingField;

    private string idField;

    private string nameField;

    /// <remarks/>
    public string incoming
    {
        get
        {
            return this.incomingField;
        }
        set
        {
            this.incomingField = value;
        }
    }

    /// <remarks/>
    public string outgoing
    {
        get
        {
            return this.outgoingField;
        }
        set
        {
            this.outgoingField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string id
    {
        get
        {
            return this.idField;
        }
        set
        {
            this.idField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string name
    {
        get
        {
            return this.nameField;
        }
        set
        {
            this.nameField = value;
        }
    }
}

