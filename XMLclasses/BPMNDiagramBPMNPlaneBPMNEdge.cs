﻿
/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.omg.org/spec/BPMN/20100524/DI")]
public partial class BPMNDiagramBPMNPlaneBPMNEdge
{

    private waypoint[] waypointField;

    private BPMNDiagramBPMNPlaneBPMNEdgeBPMNLabel bPMNLabelField;

    private string idField;

    private string bpmnElementField;

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute("waypoint", Namespace = "http://www.omg.org/spec/DD/20100524/DI")]
    public waypoint[] waypoint
    {
        get
        {
            return this.waypointField;
        }
        set
        {
            this.waypointField = value;
        }
    }

    /// <remarks/>
    public BPMNDiagramBPMNPlaneBPMNEdgeBPMNLabel BPMNLabel
    {
        get
        {
            return this.bPMNLabelField;
        }
        set
        {
            this.bPMNLabelField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string id
    {
        get
        {
            return this.idField;
        }
        set
        {
            this.idField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string bpmnElement
    {
        get
        {
            return this.bpmnElementField;
        }
        set
        {
            this.bpmnElementField = value;
        }
    }
}

