﻿
/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.omg.org/spec/BPMN/20100524/DI")]
public partial class BPMNDiagramBPMNPlaneBPMNShape
{

    private Bounds boundsField;

    private BPMNDiagramBPMNPlaneBPMNShapeBPMNLabel bPMNLabelField;

    private string idField;

    private string bpmnElementField;

    private bool isMarkerVisibleField;

    private bool isMarkerVisibleFieldSpecified;

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.omg.org/spec/DD/20100524/DC")]
    public Bounds Bounds
    {
        get
        {
            return this.boundsField;
        }
        set
        {
            this.boundsField = value;
        }
    }

    /// <remarks/>
    public BPMNDiagramBPMNPlaneBPMNShapeBPMNLabel BPMNLabel
    {
        get
        {
            return this.bPMNLabelField;
        }
        set
        {
            this.bPMNLabelField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string id
    {
        get
        {
            return this.idField;
        }
        set
        {
            this.idField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string bpmnElement
    {
        get
        {
            return this.bpmnElementField;
        }
        set
        {
            this.bpmnElementField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public bool isMarkerVisible
    {
        get
        {
            return this.isMarkerVisibleField;
        }
        set
        {
            this.isMarkerVisibleField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlIgnoreAttribute()]
    public bool isMarkerVisibleSpecified
    {
        get
        {
            return this.isMarkerVisibleFieldSpecified;
        }
        set
        {
            this.isMarkerVisibleFieldSpecified = value;
        }
    }
}

