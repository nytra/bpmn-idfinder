﻿
/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.omg.org/spec/BPMN/20100524/DI")]
public partial class BPMNDiagramBPMNPlaneBPMNEdgeBPMNLabel
{

    private Bounds boundsField;

    /// <remarks/>
    [System.Xml.Serialization.XmlElementAttribute(Namespace = "http://www.omg.org/spec/DD/20100524/DC")]
    public Bounds Bounds
    {
        get
        {
            return this.boundsField;
        }
        set
        {
            this.boundsField = value;
        }
    }
}

