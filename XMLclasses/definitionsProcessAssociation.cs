﻿
/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")]
public partial class definitionsProcessAssociation
{

    private string idField;

    private string sourceRefField;

    private string targetRefField;

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string id
    {
        get
        {
            return this.idField;
        }
        set
        {
            this.idField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string sourceRef
    {
        get
        {
            return this.sourceRefField;
        }
        set
        {
            this.sourceRefField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string targetRef
    {
        get
        {
            return this.targetRefField;
        }
        set
        {
            this.targetRefField = value;
        }
    }
}

