﻿/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.omg.org/spec/BPMN/20100524/MODEL")]
public partial class definitionsProcess
{

    private definitionsProcessTask[] itemsField;

    private string idField;

    private string nameField;

    private bool isExecutableField;

    /// <remarks/>
    //[System.Xml.Serialization.XmlElementAttribute("association", typeof(definitionsProcessAssociation))]
    //[System.Xml.Serialization.XmlElementAttribute("dataObject", typeof(definitionsProcessDataObject))]
    //[System.Xml.Serialization.XmlElementAttribute("dataObjectReference", typeof(definitionsProcessDataObjectReference))]
    //[System.Xml.Serialization.XmlElementAttribute("dataStoreReference", typeof(definitionsProcessDataStoreReference))]
    //[System.Xml.Serialization.XmlElementAttribute("exclusiveGateway", typeof(definitionsProcessExclusiveGateway))]
    //[System.Xml.Serialization.XmlElementAttribute("intermediateThrowEvent", typeof(definitionsProcessIntermediateThrowEvent))]
    //[System.Xml.Serialization.XmlElementAttribute("sequenceFlow", typeof(definitionsProcessSequenceFlow))]
    //[System.Xml.Serialization.XmlElementAttribute("serviceTask", typeof(definitionsProcessServiceTask))]
    //[System.Xml.Serialization.XmlElementAttribute("startEvent", typeof(definitionsProcessStartEvent))]
    [System.Xml.Serialization.XmlElementAttribute("task", typeof(definitionsProcessTask))]
   
    //[System.Xml.Serialization.XmlElementAttribute("textAnnotation", typeof(definitionsProcessTextAnnotation))]
    public definitionsProcessTask[] Items
    {
        get
        {
            return this.itemsField;
        }
        set
        {
            this.itemsField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string id
    {
        get
        {
            return this.idField;
        }
        set
        {
            this.idField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public string name
    {
        get
        {
            return this.nameField;
        }
        set
        {
            this.nameField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public bool isExecutable
    {
        get
        {
            return this.isExecutableField;
        }
        set
        {
            this.isExecutableField = value;
        }
    }
}

